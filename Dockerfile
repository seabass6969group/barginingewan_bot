FROM archlinux:latest
ENV WORKDIR "/work"
ENV ACCESS_TOKEN "secrets"
RUN pacman -Syu curl ffmpeg nodejs npm make yt-dlp --noconfirm
# RUN curl -fsSL https://deb.nodesource.com/setup_21.x | bash - && apt install -y nodejs
COPY . /work
WORKDIR ${WORKDIR}
RUN npm install
CMD npm run dev


