import { config } from "../config";

export async function checkexist(userID: string) {
	let myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");
	// myHeaders.append("X-access-token", config.ACCESS_TOKEN)
	const existed = await fetch(
		`http://${config.LOCAL_SERVER}/api/private/checkExist`,
		{
			method: "POST",
			headers: myHeaders,
			body: JSON.stringify({
				userID: userID,
				TOKEN: config.ACCESS_TOKEN,
			}),
			redirect: "follow",
		},
	);
	const existjson = await existed.json();
	const exist = existjson.exist;
	return exist;
}
