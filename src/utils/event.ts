import { config } from "../config";
import { checkexist } from "./checkUserExist";

export enum eventtype {
	nicknameChanger = "nicknameChanger",
}
export class userEvent {
	userCreator: string;
	userEffect: string;
	expiryDate: Date;
	eventType: eventtype;
	constructor(
		userCreator: string,
		expiryLengthHour: number,
		eventType: eventtype,
		userEffect: string,
	) {
		this.userCreator = userCreator;
		this.userEffect = userEffect;
		this.expiryDate = new Date(
			new Date().getTime() + 1000 * 60 * 60 * expiryLengthHour,
		);
		this.eventType = eventType;
		// server side post event
	}
	async verification(): Promise<boolean> {
		if (
			(await checkexist(this.userCreator)) == "yes" &&
			(await checkexist(this.userEffect)) == "yes"
		) {
			return true;
		} else {
			return false;
		}
	}
	async postRecords() {
		let myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		await fetch(`http://${config.LOCAL_SERVER}/api/private/eventPoster`, {
			method: "POST",
			// headers: new Headers().append("Content-Type", "application/json"),
			headers: myHeaders,
			body: JSON.stringify({
				userCreator: this.userCreator,
				userEffect: this.userEffect,
				expiryDate: this.expiryDate,
				eventType: this.eventType,
				eventInfo: {},
				TOKEN: config.ACCESS_TOKEN,
			}),
			redirect: "follow",
		});
	}
}
