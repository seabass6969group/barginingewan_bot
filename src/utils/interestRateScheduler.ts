import { WebhookClient } from "discord.js";
import { schedule } from "node-cron";
import { config } from "../config";
import { log } from "./logging";
export function interestSchedular() {
	schedule("0 8 * * *", async () => {
		let myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		// myHeaders.append("X-access-token", config.ACCESS_TOKEN)
		const webhook = new WebhookClient({
			url: config.WEBHOOK_URL,
		});
		let today_interest = Math.floor(Math.random() * 7 + 1) * 0.5;
		await webhook.send({
			content: `Todays Interest Rate is: ${today_interest}%`,
		});

		await fetch(`http://${config.LOCAL_SERVER}/api/private/interestRate`, {
			method: "POST",
			// headers: new Headers().append("Content-Type", "application/json"),
			headers: myHeaders,
			body: JSON.stringify({
				interest: today_interest / 100,
				TOKEN: config.ACCESS_TOKEN,
			}),
			redirect: "follow",
		});

		await log("Auto: Daily Interest Rate", "000");
	});
}
