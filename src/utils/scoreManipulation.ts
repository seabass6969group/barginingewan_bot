import { config } from "../config";

export async function scoreManipulation(userID: string, score: number) {
	var raw = JSON.stringify({
		userID: userID,
		score: score,
		TOKEN: config.ACCESS_TOKEN,
	});

	let myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");
	// myHeaders.append("X-access-token", config.ACCESS_TOKEN)
	var requestOptions = {
		method: "POST",
		body: raw,
		headers: myHeaders,
		redirect: "follow",
	};

	const result = await fetch(
		"http://" + config.LOCAL_SERVER + "/api/private/appendScore",
		requestOptions,
	);
}
