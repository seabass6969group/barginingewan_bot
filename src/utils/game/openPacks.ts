import { config } from "../../config";
import {
	chanceModifier,
	marketplaceItems,
} from "../../database/marketplaceItems";
import { randomNumber } from "../random";

type rarity = "Godly" | "Mythic" | "Legendary" | "Rare" | "Uncommon" | "Common";

type responseData = {
	inventory: {
		cardsId: string;
		cardsRarity: rarity;
		inventory: number;
		imageUrl: string;
		cardsType: "MainCard" | "RizzCard" | "AbilityCard";
	}[];
};
export async function openPacks(marketplaceItem: string, userID: string) {
	const marketplace = marketplaceItems.find((t) => t.value == marketplaceItem);
	if (marketplace == undefined) {
		throw new Error("Not an items");
	}
	for (let index = 0; index < marketplace.number; index++) {
		await openOnePack(
			marketplace.rizz,
			marketplace.main,
			marketplace.ability,
			userID,
			marketplace.chance,
		);
	}
}
export async function openOnePack(
	rizz = true,
	main = true,
	ability = true,
	userID: string,
	chance: chanceModifier,
) {
	let myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");
	let body: { TOKEN: string | undefined; cardsType: string[] } = {
		TOKEN: config.ACCESS_TOKEN,
		cardsType: [],
	};
	if (rizz) {
		body.cardsType.push("RizzCard");
	}
	if (main) {
		body.cardsType.push("MainCard");
	}
	if (ability) {
		body.cardsType.push("AbilityCard");
	}
	const inventoryFetch = await fetch(
		`http://${config.LOCAL_SERVER}/api/private/InventoryStatusCardOnlyPositive`,
		{
			method: "POST",
			headers: myHeaders,
			body: JSON.stringify(body),
			redirect: "follow",
		},
	);
	const inventoryJson = await inventoryFetch.json();
	if (inventoryFetch.status == 201) {
		const inventory = inventoryJson as responseData;
		let total_cards = 0;
		let cards_random: { id: string; inventory: number }[] = [];
		inventory.inventory.forEach((e) => {
			total_cards += e.inventory;
			switch (e.cardsRarity) {
				case "Godly":
					cards_random.push({ id: e.cardsId, inventory: chance.Godly });
					break;
				case "Mythic":
					cards_random.push({ id: e.cardsId, inventory: chance.Mythic });
					break;
				case "Legendary":
					cards_random.push({ id: e.cardsId, inventory: chance.Legendary });
					break;
				case "Rare":
					cards_random.push({ id: e.cardsId, inventory: chance.Rare });
					break;
				case "Uncommon":
					cards_random.push({ id: e.cardsId, inventory: chance.Uncommon });
					break;
				case "Common":
					cards_random.push({ id: e.cardsId, inventory: chance.Common });
					break;
			}
		});
		const random = randomNumber(0, total_cards);
		function returnLocation(randomDegit: number) {
			let location = 0;
			let passingOut = 0;
			cards_random.forEach((e, index) => {
				if (location < randomDegit && location + e.inventory > randomDegit) {
					passingOut = index;
					return;
				}
				location += e.inventory;
			});
			return cards_random[passingOut].id;
		}
		const purchasingCard = await fetch(
			`http://${config.LOCAL_SERVER}/api/private/purchasingCard`,
			{
				method: "POST",
				headers: myHeaders,
				body: JSON.stringify({
					TOKEN: config.ACCESS_TOKEN,
					userID: userID,
					id: returnLocation(random),
				}),
				redirect: "follow",
			},
		);
		const purchasingCardJson = await purchasingCard.json();
		if (purchasingCardJson["done"] == "done") {
			return inventory.inventory[random];
		} else {
			throw new Error(await purchasingCard.json());
		}
	} else {
		throw new Error("Not responding with 201");
	}
}
