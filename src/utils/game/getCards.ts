import { config } from "../../config";

export async function getOwnedCards(userID: string) {
	let myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");
	// myHeaders.append("X-access-token", config.ACCESS_TOKEN)
	const existed = await fetch(
		`http://${config.LOCAL_SERVER}/api/private/getUserCards`,
		{
			method: "POST",
			headers: myHeaders,
			// headers: new Headers().append("Content-Type", "application/json"),
			body: JSON.stringify({
				userID: userID,
				TOKEN: config.ACCESS_TOKEN,
			}),
			redirect: "follow",
		},
	);
	if (existed.status == 201){
		const existjson = await existed.json();
		const exist = existjson.cards.ownedCards;
		return exist;
	}else{
		throw new Error("Something broken")
	}
}
