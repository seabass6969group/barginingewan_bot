import * as leaderboard from "../commands/Game/getGeneralInfo/leaderboard";
import * as appendScore from "../commands/Game/Admin/appendScore";
import * as randomEwan from "../commands/imageEdit/randomEwan";
import * as ewanIndex from "../commands/imageEdit/ewanIndex";
import * as chooseandpaint from "../commands/imageEdit/chooseandpaint";
import * as gambling from "../commands/Game/gambling";
import * as advancedewanedit from "../commands/imageEdit/advancedEwanedit";
import * as newUser from "../commands/Game/userCommand/newUser";
import * as yourscore from "../commands/Game/getGeneralInfo/yourscore";
import * as interestRateinfo from "../commands/Game/getGeneralInfo/interestRate";
import * as transfer from "../commands/Game/userCommand/atomicTransfer";
import * as tax from "../commands/Game/getGeneralInfo/tax";
import * as getCreeped from "../commands/VC/getCreeped";
import * as message from "../commands/Game/Admin/sendMessage";
import * as stopCreeping from "../commands/VC/stopCreepingOnMe";
// import * as admin from "./../commands/admin"
import * as queue from "../commands/VC/songQueue";
import * as french_vocab from "../commands/utility/frenchVocab";
import * as spanish_vocab from "../commands/utility/spanishVocab";
import * as nicknameChanger from "../commands/Game/externalPermission/nicknameChanger";
import * as cards from "../commands/Game/cardGame/Cards";
import * as marketplace from "../commands/Game/marketplace";
import * as myCards from "../commands/Game/cardGame/Mycards";

export const commands = [
	leaderboard,
	appendScore,
	randomEwan,
	ewanIndex,
	chooseandpaint,
	gambling,
	advancedewanedit,
	newUser,
	yourscore,
	interestRateinfo,
	transfer,
	tax,
	getCreeped,
	message,
	stopCreeping,
	queue,
	french_vocab,
	spanish_vocab,
	nicknameChanger,
	cards,
	marketplace,
	myCards,

	// admin
];
