import { config } from "../config";
export async function log(activity: string, userID: string) {
	var raw = JSON.stringify({
		userID: userID,
		activity: activity,
		TOKEN: config.ACCESS_TOKEN,
	});

	let myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");
	// myHeaders.append("X-access-token", config.ACCESS_TOKEN)
	var requestOptions = {
		method: "POST",
		body: raw,
		headers: myHeaders,
		redirect: "follow",
	};

	const result = await fetch(
		"http://" + config.LOCAL_SERVER + "/api/private/logging",
		requestOptions,
	);
}
