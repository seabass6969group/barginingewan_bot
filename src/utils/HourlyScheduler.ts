import { schedule } from "node-cron";
import { config } from "../config";
import { Client } from "discord.js";

export function HourlySchedular(Client: Client) {
	schedule("0 * * * *", async () => {
		let myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		const values = await fetch(
			`http://${config.LOCAL_SERVER}/api/private/getEvent`,
			{
				method: "POST",
				// headers: new Headers().append("Content-Type", "application/json"),
				headers: myHeaders,
				body: JSON.stringify({ TOKEN: config.ACCESS_TOKEN }),
				redirect: "follow",
			},
		);
		const compiled = await values.json();
		if (values.status == 201) {
			compiled.content.forEach(async (element) => {
				if (
					new Date(element.expiryDate).getTime() - new Date().getTime() <=
					0
				) {
					const deleter = await fetch(
						`http://${config.LOCAL_SERVER}/api/private/deleteEvent`,
						{
							method: "POST",
							// headers: new Headers().append("Content-Type", "application/json"),
							headers: myHeaders,
							body: JSON.stringify({
								TOKEN: config.ACCESS_TOKEN,
								_id: element._id,
							}),
							redirect: "follow",
						},
					);
					try {
						const person = Client.guilds.cache
							.get(config.DISCORD_GUILD_ID)
							?.members.cache.get(element.userEffect);
						await person?.setNickname(person?.displayName);
					} catch (error) {
						console.log("error when reverting nickname");
					}
				}
			});
		}
	});
}
