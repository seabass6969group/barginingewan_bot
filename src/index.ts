import {
	ActivityType,
	Client,
	Events,
	GatewayIntentBits,
	WebhookClient,
} from "discord.js";
import { config } from "./config";
import { deployCommands } from "./utils/registerCommand";
import { commands } from "./utils/commands";
import Jimp from "jimp";
import { v4 as uuidv4 } from "uuid";
import { image_url } from "./database/image";
import { unlink } from "fs";
import { interestSchedular } from "./utils/interestRateScheduler";
import { HourlySchedular } from "./utils/HourlyScheduler";

// Create a new client instance
const client = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.DirectMessages,
		GatewayIntentBits.GuildVoiceStates,
	],
});

// When the client is ready, run this code (only once).
// The distinction between `client: Client<boolean>` and `readyClient: Client<true>` is important for TypeScript developers.
// It makes some properties non-nullable.
client.once(Events.ClientReady, async (readyClient) => {
	console.log(`Ready! Logged in as ${readyClient.user.tag}`);
	await deployCommands({ guildId: config.DISCORD_GUILD_ID });
	client.user?.setActivity({
		name: "Ewan",
		type: ActivityType.Playing,
		url: "https://ewan.cephas.monster",
	});
	interestSchedular();
	HourlySchedular(readyClient);
});

client.on("guildCreate", async (guild) => {});
client.on("interactionCreate", async (interaction) => {
	if (interaction.isCommand()) {
		const { commandName } = interaction;
		const index = commands.findIndex(
			(command) => command.data.name == commandName,
		);
		if (commands[index]) {
			try {
				commands[index].execute(interaction);
			} catch (error) {
				interaction.channel?.send(
					"a super unlikely bug have happen! It might be catastrophic so be careful",
				);
				console.log("......................................................");
				console.log(
					interaction.user.username +
						"issued a command of: " +
						interaction.commandName,
				);
				console.log("and it has cause an error of: ");
				console.log(error);
				console.log("......................................................");
				console.log(error);
			}
		}
	} else if (interaction.isModalSubmit()) {
		if (interaction.customId.includes("textModal_")) {
			const field = interaction.fields.getTextInputValue("text");
			const imageUUID = "/tmp/" + uuidv4() + ".jpg";
			const index = parseInt(interaction.customId.replace("textModal_", ""));
			const image_on = image_url[index].replace("./", "").toString();
			const image = "https://ewan.cephas.monster/" + image_on;
			Jimp.read(image)
				.then(async (file) => {
					const text = field;
					const loadFont = await Jimp.loadFont(Jimp.FONT_SANS_128_WHITE);
					const textHeight = await Jimp.measureTextHeight(
						loadFont,
						text,
						file.bitmap.width,
					);
					let transparentImg = new Jimp(
						file.bitmap.width,
						textHeight,
						"#000000ff",
						(err, image) => {},
					);
					transparentImg.opacity(0.8);
					file.composite(
						transparentImg,
						0,
						file.bitmap.height / 2 - textHeight / 2,
					);
					file.print(
						await loadFont,
						0,
						0,
						{
							text: text,
							alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
							alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE,
						},
						file.bitmap.width,
						file.bitmap.height,
					);
					await file.writeAsync(imageUUID); // save
					console.log(imageUUID);
					await interaction.reply({ files: [imageUUID] });
					await unlink(imageUUID, (err) => {});
				})
				.catch((err) => {
					console.error(err);
				});
		}
	} else {
		return;
	}
});

// Log in to Discord with your client's token
client.login(config.DISCORD_TOKEN);
