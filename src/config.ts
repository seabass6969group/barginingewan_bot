import dotenv from "dotenv";

dotenv.config();

const {
	DISCORD_TOKEN,
	DISCORD_GUILD_ID,
	DISCORD_CLIENT_ID,
	LOCAL_SERVER,
	WEBHOOK_URL,
	ACCESS_TOKEN,
} = process.env;

if (
	!DISCORD_TOKEN &&
	!DISCORD_GUILD_ID &&
	!DISCORD_CLIENT_ID &&
	!LOCAL_SERVER &&
	!WEBHOOK_URL &&
	!ACCESS_TOKEN
) {
	throw new Error("Missing environment variables");
}

export const config = {
	DISCORD_TOKEN,
	DISCORD_CLIENT_ID,
	DISCORD_GUILD_ID,
	LOCAL_SERVER,
	WEBHOOK_URL,
	ACCESS_TOKEN,
};
