import { CommandInteraction, SlashCommandBuilder } from "discord.js";

export const data = new SlashCommandBuilder()
	.setName("ewan-index")
	.setDescription("show the index website");

export async function execute(interaction: CommandInteraction) {
	return interaction.reply(
		"The photo index correspond to https://ewan.cephas.monster/galleryWithindex",
	);
}
