import {
	ActionRow,
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	CommandInteraction,
	ComponentType,
	ModalActionRowComponentBuilder,
	ModalBuilder,
	SlashCommandBuilder,
	TextInputBuilder,
	TextInputStyle,
} from "discord.js";
import { image_url } from "../../database/image";

export const data = new SlashCommandBuilder()
	.setName("randomewan")
	.setDescription("Ewan is feeling lucky today!");

export async function execute(interaction: CommandInteraction) {
	// Returns a random integer from 0 to 9:
	let index = Math.floor(Math.random() * (image_url.length - 1));
	const image_on = image_url[index].replace("./", "").toString();
	const image = "https://ewan.cephas.monster/" + image_on;
	const row = new ActionRowBuilder<ButtonBuilder>({
		components: [
			{
				custom_id: "download",
				label: "⬇️",
				style: ButtonStyle.Primary,
				type: ComponentType.Button,
			},
			{
				custom_id: "edit",
				label: "🖊️",
				style: ButtonStyle.Primary,
				type: ComponentType.Button,
			},
		],
	});
	const response = await interaction.reply({
		content: image,
		components: [row],
	});
	try {
		const confirmation = await response.awaitMessageComponent({
			time: 60_000,
		});
		if (confirmation.customId === "download") {
			await confirmation.update({ content: image, components: [] });
			await interaction.followUp({
				content: "[Download link](<" + image + ">)",
				components: [],
			});
		} else {
			const TextInput = new TextInputBuilder()
				.setLabel("what text do you want it to have?")
				.setStyle(TextInputStyle.Paragraph)
				.setCustomId("text");
			const data = new ModalBuilder()
				.setTitle("image info")
				.addComponents(
					new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
						TextInput,
					),
				)
				.setCustomId("textModal_" + index);
			await confirmation.showModal(data);
		}
	} catch (e) {
		console.log(e);
		await interaction.editReply({ content: image, components: [] });
	}
}
