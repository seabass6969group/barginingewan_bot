import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { image_url } from "../../database/image";
import { v4 as uuidv4 } from "uuid";
import Jimp from "jimp";
import { unlink } from "fs";

export const data = new SlashCommandBuilder()
	.setName("advanced-ewan-edit")
	.setDescription("test")
	.addSubcommand((command) =>
		command
			.setName("fisheye")
			.setDescription("add fish eye to the image")
			.addStringOption((option) =>
				option
					.setName("fisheyevalue")
					.setDescription("fish eye radius 0 to 20")
					.setRequired(true),
			)
			.addStringOption((option) =>
				option
					.setName("photoindex")
					.setDescription("Index of photo. Further detail see /ewan-index")
					.setRequired(true),
			),
	)
	.addSubcommand((command) =>
		command
			.setName("blur")
			.setDescription("Blur the image")
			.addStringOption((option) =>
				option
					.setName("blurvalue")
					.setDescription("blur value 0 to 20")
					.setRequired(true),
			)
			.addStringOption((option) =>
				option
					.setName("photoindex")
					.setDescription("Index of photo. Further detail see /ewan-index")
					.setRequired(true),
			),
	)
	// .addSubcommand(command =>
	//     command
	//         .setName("gaussian")
	//         .setDescription("Intense Blur the image")
	//         .addStringOption(
	//             option => option.setName("blurvalue").setDescription("blur value 0 to 20").setRequired(true)
	//         )
	//         .addStringOption(option => option.setName("photoindex").setDescription("Index of photo. Further detail see /ewan-index").setRequired(true))
	// )
	.addSubcommand((command) =>
		command
			.setName("flip")
			.setDescription("flip the image")
			.addStringOption((option) =>
				option
					.setName("flipside")
					.setDescription("horizontally or vertically")
					.addChoices(
						{ name: "horizontally", value: "x" },
						{ name: "vertically", value: "y" },
					)
					.setRequired(true),
			)
			.addStringOption((option) =>
				option
					.setName("photoindex")
					.setDescription("Index of photo. Further detail see /ewan-index")
					.setRequired(true),
			),
	);

enum operator {
	blur = "blur",
	flip = "flip",
	fisheye = "fisheye",
	// gaussian = "gaussian"
}
export async function execute(interaction: CommandInteraction) {
	const option: string = interaction.options.getString("photoindex");
	const checker =
		parseFloat(interaction.options.getString("fisheyevalue")) ||
		parseInt(interaction.options.getString("blurvalue")) ||
		interaction.options.getString("flipside") == "x" ||
		interaction.options.getString("flipside") == "y";
	if (
		parseInt(option) !== undefined &&
		parseInt(option) >= 0 &&
		parseInt(option) < image_url.length &&
		checker
	) {
		const imageUUID = "/tmp/" + uuidv4() + ".jpg";
		const index = parseInt(option);
		const image_on = image_url[index].replace("./", "").toString();
		const image = "https://ewan.cephas.monster/" + image_on;
		const loading_reply = await interaction.reply("Loading...");
		Jimp.read(image)
			.then(async (file) => {
				switch (interaction.options.getSubcommand()) {
					case operator.blur:
						file.blur(parseInt(interaction.options.getString("blurvalue")));
						break;
					case operator.fisheye:
						file.fisheye({
							r: parseFloat(interaction.options.getString("fisheyevalue")),
						});
						break;
					case operator.flip:
						if (interaction.options.getString("flipside") == "x") {
							file.flip(true, false);
						} else {
							file.flip(false, true);
						}
						break;
					// case operator.gaussian:
					//     file.gaussian(parseInt(interaction.options.getString("blurvalue")))
					//     break;
					default:
						interaction.reply({
							content: "Invalid Input",
							ephemeral: true,
						});
						break;
				}
				await file.writeAsync(imageUUID); // save
				console.log(imageUUID);
				await interaction.channel?.sendTyping();
				await interaction.followUp({ files: [imageUUID] });
				await loading_reply.delete();
				await unlink(imageUUID, (err) => {});
			})
			.catch((err) => {
				interaction.followUp({
					content: "Opss something appeared to have broken.",
					ephemeral: true,
				});
				console.log("......................................................");
				console.log(
					interaction.user.username +
						"issued a command of: " +
						interaction.commandName,
				);
				console.log("and it has cause an error of: ");
				console.log(err);
				console.log("......................................................");
			});
	} else {
		interaction.reply({ content: "Invalid Input", ephemeral: true });
	}
}
