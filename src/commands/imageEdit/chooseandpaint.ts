import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { image_url } from "../../database/image";
import { v4 as uuidv4 } from "uuid";
import Jimp from "jimp";
import { unlink } from "fs";

export const data = new SlashCommandBuilder()
	.setName("edit-ewan")
	.setDescription("edit a picture of Ewan")
	.addStringOption((option) =>
		option
			.setName("photoindex")
			.setDescription("Index of photo. Further detail see /ewan-index")
			.setRequired(true),
	)
	.addStringOption((option) =>
		option
			.setName("imagecontent")
			.setDescription("The content of the image")
			.setRequired(true),
	);

export async function execute(interaction: CommandInteraction) {
	const option: string = interaction.options.getString("photoindex");
	const text: string = interaction.options.getString("imagecontent");
	if (
		parseInt(option) !== undefined &&
		parseInt(option) >= 0 &&
		parseInt(option) < image_url.length
	) {
		const imageUUID = "/tmp/" + uuidv4() + ".jpg";
		const index = parseInt(option);
		const image_on = image_url[index].replace("./", "").toString();
		const image = "https://ewan.cephas.monster/" + image_on;
		await interaction.channel?.sendTyping();
		const loading_reply = await interaction.reply("Loading...");
		Jimp.read(image)
			.then(async (file) => {
				const loadFont = await Jimp.loadFont(Jimp.FONT_SANS_128_WHITE);
				const textHeight = await Jimp.measureTextHeight(
					loadFont,
					text,
					file.bitmap.width,
				);
				let transparentImg = new Jimp(
					file.bitmap.width,
					textHeight,
					"#000000ff",
					(err, image) => {},
				);
				transparentImg.opacity(0.8);
				file.composite(
					transparentImg,
					0,
					file.bitmap.height / 2 - textHeight / 2,
				);
				file.print(
					await loadFont,
					0,
					0,
					{
						text: text,
						alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
						alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE,
					},
					file.bitmap.width,
					file.bitmap.height,
				);
				await file.writeAsync(imageUUID); // save
				console.log(imageUUID);
				await interaction.followUp({ files: [imageUUID] });
				await loading_reply.delete();
				await unlink(imageUUID, (err) => {});
			})
			.catch((err) => {
				interaction.followUp({
					content: "Opss something appeared to have broken.",
					ephemeral: true,
				});
				console.log("......................................................");
				console.log(
					interaction.user.username +
						"issued a command of: " +
						interaction.commandName,
				);
				console.log("and it has cause an error of: ");
				console.log(err);
				console.log("......................................................");
			});
	} else {
		interaction.reply({ content: "Invalid Input", ephemeral: true });
	}
}
