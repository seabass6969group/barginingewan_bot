import { getVoiceConnection } from "@discordjs/voice";
import { CommandInteraction, SlashCommandBuilder } from "discord.js";

export const data = new SlashCommandBuilder()
	.setName("leave-vc")
	.setDescription("leave the voice channel");

export async function execute(interaction: CommandInteraction) {
	let connection = getVoiceConnection(interaction.guild.id);
	if (!connection) {
		return interaction.reply("Sorry I am not in a voice channel!");
	}
	connection.destroy();
	return interaction.reply("I am leaving the channel. OK.");
}
