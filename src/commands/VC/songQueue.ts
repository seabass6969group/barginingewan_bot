// incomplete
import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { songs } from "../../database/song";

export const data = new SlashCommandBuilder()
	.setName("queue")
	.setDescription("show the song queue");

export async function execute(interaction: CommandInteraction) {
	if (songs.length == 0) {
		return interaction.reply("I don't have song in the queue");
	}
	let compiledNiceLooking: string[] = [];
	songs.forEach((song) => {
		compiledNiceLooking.push(
			`- ${song.audioResource.metadata.title} and added by ${song.addedBy.displayName}`,
		);
	});
	return interaction.reply(compiledNiceLooking.join("\n"));
}
