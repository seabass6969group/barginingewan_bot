// incomplete
import {
	AudioPlayerStatus,
	VoiceConnection,
	VoiceConnectionStatus,
	createAudioPlayer,
	createAudioResource,
	joinVoiceChannel,
} from "@discordjs/voice";
import {
	ChannelType,
	CommandInteraction,
	SlashCommandBuilder,
} from "discord.js";
import { exec } from "child_process";
import { v4 as uuidv4 } from "uuid";
import { songs } from "../../database/song";
export const data = new SlashCommandBuilder()
	.setName("playaudio")
	.setDescription("specify a voice channel and get creeped")
	.addChannelOption((option) =>
		option
			.addChannelTypes(ChannelType.GuildVoice)
			.setName("voice-channel")
			.setDescription("The voice channel you want it to be joined in.")
			.setRequired(true),
	)
	.addStringOption((option) =>
		option
			.setName("search")
			.setDescription("The song name or a valid youtube link")
			.setRequired(true),
	);

export async function execute(interaction: CommandInteraction) {
	if (
		!interaction.options.getChannel("voice-channel") &&
		!interaction.options.getString("search")
	) {
		return interaction.reply(
			"You are not specifying the channel or the search term. You are not going to get creep :cry:.",
		);
	}
	const searchString = interaction.options.getString("search");
	let testResult: boolean = /^([A-Za-z 0-9]+$)/.test(searchString);
	let youtubeLink: boolean =
		/https:\/\/www\.youtube.com\/watch\?v=([A-Za-z 0-9])+$/.test(searchString);
	if (testResult == false && youtubeLink == false) {
		return interaction.reply(
			"You are not allow to use special character in your search terms unless is a youtube video link for example <https://www.youtube.com/watch?v=Cc25FrPjlZs>. You are not going to get creep :cry:.",
		);
	}
	interaction.reply("Doing!");
	try {
		const connection = joinVoiceChannel({
			channelId: interaction.options.getChannel("voice-channel").id,
			guildId: interaction.guildId,
			adapterCreator: interaction.guild?.voiceAdapterCreator,
		});

		const audioUUID = `/tmp/${uuidv4()}.mp3`;
		const player = createAudioPlayer();
		if (youtubeLink) {
			exec(
				`yt-dlp '${searchString}' -f "ba" -o '${audioUUID}'`,
				async (err) => {
					if (err) {
						await interaction.channel?.send("Unexpected error occured sorry!");
					}
					let videoTitle = "";
					exec(`yt-dlp '${searchString}' --get-title`, (err, stdout, _) => {
						if (err) {
							videoTitle = searchString;
						}
						videoTitle = stdout;
					});
					const audioResource = createAudioResource(audioUUID, {
						metadata: { title: videoTitle },
					});
					audioResource.volume?.setVolume(0.5);
					songs.push({
						addedBy: interaction.user,
						audioResource: audioResource,
					});
					// if (player.state.status != AudioPlayerStatus.Playing) {
					//     connection.subscribe(player)
					//     player.play(songs[0].audioResource)
					// }
					interaction.channel?.send(
						`Now playing: ${songs[0].audioResource.metadata.title} From ${songs[0].addedBy.displayName}`,
					);
					interaction.channel?.send(`Successfully added song ${videoTitle}`);
					player.play(songs[0].audioResource);
					songs.shift();
					connection.subscribe(player);
				},
			);
		} else {
			exec(
				`yt-dlp 'ytsearch1:${searchString}' -f "ba" -o '${audioUUID}'`,
				async (err, stdout, stderr) => {
					if (err) {
						await interaction.channel?.send("Unexpected error occured sorry!");
					}
					let videoTitle = "";
					exec(`yt-dlp '${searchString}' --get-title`, (err, stdout, _) => {
						if (err) {
							videoTitle = searchString;
						}
						videoTitle = stdout;
					});
					const audioResource = createAudioResource(audioUUID, {
						metadata: { title: videoTitle },
					});
					audioResource.volume?.setVolume(0.5);
					songs.push({
						addedBy: interaction.user,
						audioResource: audioResource,
					});
					connection.subscribe(player);
					interaction.channel?.send(`Successfully added song ${videoTitle}`);
					// await interaction.channel?.send("stdout: " + stdout.toString())
					player.play(songs[0].audioResource);
					songs.shift();
					connection.subscribe(player);
				},
			);
		}
		player.on(AudioPlayerStatus.Playing, () => {
			console.log("The audio player has started playing!");
		});
	} catch (error) {
		console.log("......................................................");
		console.log(
			interaction.user.username +
				"issued a command of: " +
				interaction.commandName,
		);
		console.log("and it has cause an error of: ");
		console.log(error);
		console.log("......................................................");
		return interaction.channel?.send("the bot got Creeped nooooo :cry:");
	}
}
