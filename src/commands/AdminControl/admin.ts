import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { log } from "../../utils/logging";
import { exec } from "child_process";
export const data = new SlashCommandBuilder()
	.setName("admin")
	.setDescription("admin")
	.addStringOption((option) =>
		option.setName("command").setDescription("command").setRequired(true),
	);

export async function execute(interaction: CommandInteraction) {
	const commands = interaction.options.getString("command");
	if (interaction.member.roles.cache.has("1208542738388357150")) {
		if (interaction.user.id == "428949783919460363") {
			await log("Issued: Command - admin", interaction.user.id);
			return await interaction.reply("lack of permission ");
		} else {
			await log("Issued: Command - admin", interaction.user.id);
			exec(commands, async (err, stdout, stderr) => {
				if (err) {
					await interaction.channel?.send("error in: " + err.toString());
				}
				await interaction.channel?.send("stdout: " + stdout.toString());
				await interaction.channel?.send("stderr: " + stderr.toString());
			});
			await interaction.reply("success");
		}
	} else {
		return interaction.reply("lack of permission!");
	}
}
