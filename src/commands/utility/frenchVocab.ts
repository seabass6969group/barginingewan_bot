import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	CommandInteraction,
	ComponentType,
	SlashCommandBuilder,
} from "discord.js";
import { french_vocab } from "../../database/french_vocab";

export const data = new SlashCommandBuilder()
	.setName("french-vocab")
	.setDescription("a random french word in the french aqa gcse specs");

export async function execute(interaction: CommandInteraction) {
	const random = french_vocab[Math.floor(Math.random() * french_vocab.length)];
	const row = new ActionRowBuilder<ButtonBuilder>({
		components: [
			{
				custom_id: "yes",
				label: "✅️",
				style: ButtonStyle.Primary,
				type: ComponentType.Button,
			},
			{
				custom_id: "no",
				label: "❌️",
				style: ButtonStyle.Secondary,
				type: ComponentType.Button,
			},
		],
	});
	const response = await interaction.reply({
		content: `French: ${random.french}`,
		components: [row],
	});
	try {
		const confirmation = await response.awaitMessageComponent({
			time: 60_000,
		});
		if (confirmation.customId == "yes") {
			await response.edit({
				content: `French: ${random.french}`,
				components: [],
			});
			await interaction.channel?.send(`Oui! Trés bien!`);
			await interaction.channel?.send(`English: ${random.english} `);
			await interaction.channel?.send(
				`Theme: ${random.theme} - ${random.topic_name}`,
			);
		} else if (confirmation.customId == "no") {
			await response.edit({
				content: `French: ${random.french}`,
				components: [],
			});
			await interaction.channel?.send(
				`Non! Tu es trés mauvais! Essayez encore!`,
			);
			await interaction.channel?.send(`English: ${random.english} `);
			await interaction.channel?.send(
				`Theme: ${random.theme} - ${random.topic_name}`,
			);
		}
	} catch (error) {
		await response.edit({
			content: `French: ${random.french}`,
			components: [],
		});
	}
}
