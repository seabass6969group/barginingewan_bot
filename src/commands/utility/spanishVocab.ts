import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	CommandInteraction,
	ComponentType,
	SlashCommandBuilder,
} from "discord.js";
import { spanish_vocab } from "../../database/spanish_vocab";

export const data = new SlashCommandBuilder()
	.setName("spanish-vocab")
	.setDescription("a random spanish word in the spanish aqa gcse specs");

export async function execute(interaction: CommandInteraction) {
	const random =
		spanish_vocab[Math.floor(Math.random() * spanish_vocab.length)];
	const row = new ActionRowBuilder<ButtonBuilder>({
		components: [
			{
				custom_id: "yes",
				label: "✅️",
				style: ButtonStyle.Primary,
				type: ComponentType.Button,
			},
			{
				custom_id: "no",
				label: "❌️",
				style: ButtonStyle.Secondary,
				type: ComponentType.Button,
			},
		],
	});
	const response = await interaction.reply({
		content: `Spanish: ${random.spanish}`,
		components: [row],
	});
	try {
		const confirmation = await response.awaitMessageComponent({
			time: 60_000,
		});
		if (confirmation.customId == "yes") {
			await response.edit({
				content: `Spanish: ${random.spanish}`,
				components: [],
			});
			await interaction.channel?.send(`English: ${random.english} `);
		} else if (confirmation.customId == "no") {
			await response.edit({
				content: `Spanish: ${random.spanish}`,
				components: [],
			});
			await interaction.channel?.send(`NOOOOOOO you didn't get it!`);
			await interaction.channel?.send(`English: ${random.english} `);
		}
	} catch (error) {
		await response.edit({
			content: `Spanish: ${random.spanish}`,
			components: [],
		});
	}
}
