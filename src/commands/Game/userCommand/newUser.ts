import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { config } from "../../../config";
import { checkexist } from "../../../utils/checkUserExist";

export const data = new SlashCommandBuilder()
	.setName("new-user")
	.setDescription("new user.");

export async function execute(interaction: CommandInteraction) {
	const res = await interaction.reply("Adding new user...");
	const exist = await checkexist(interaction.user.id);
	if (exist != "yes") {
		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		// myHeaders.append("X-access-token", config.ACCESS_TOKEN)

		var raw = JSON.stringify({
			name: interaction.user.displayName,
			userID: interaction.user.id,
			score: 50,
			MinecraftUUID: "NULL",

			TOKEN: config.ACCESS_TOKEN,
		});

		var requestOptions = {
			method: "POST",
			headers: myHeaders,
			body: raw,
			redirect: "follow",
		};

		fetch(`http://${config.LOCAL_SERVER}/api/private/newUser`, requestOptions)
			.then((response) => response.text())
			.then((result) => console.log(result))
			.catch((error) => console.log("error", error));
		res.delete();
		interaction.channel?.send(
			"You have been created. And your starting balace is 50!",
		);
	} else {
		res.delete();
		interaction.channel?.send("You are already existed. ");
	}
}
