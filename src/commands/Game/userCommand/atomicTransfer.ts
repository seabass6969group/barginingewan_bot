import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { config } from "../../../config";
import { scoreManipulation } from "../../../utils/scoreManipulation";
import { checkexist } from "../../../utils/checkUserExist";
import { log } from "../../../utils/logging";
export const data = new SlashCommandBuilder()
	.setName("transfer")
	.setDescription("transfer money")
	.addUserOption((option) =>
		option.setName("people").setDescription("to who").setRequired(true),
	)
	.addIntegerOption((option) =>
		option.setName("howmuch").setDescription("how much").setRequired(true),
	);

export async function execute(interaction: CommandInteraction) {
	const transfer_people = interaction.options.getUser("people");
	const transfer_amount = interaction.options.getInteger("howmuch");
	if ((await checkexist(transfer_people?.id)) != "yes") {
		return await interaction.reply(
			`${transfer_people?.displayName} appear to doesn't exist to the system!`,
		);
	}
	if (transfer_amount <= 0) {
		return await interaction.reply(`Not possible to transfer negative value!`);
	}
	if (transfer_people?.id == interaction.user.id) {
		return await interaction.reply(
			`Not possible to create a money laundarying scheme!`,
		);
	}
	await scoreManipulation(transfer_people?.id, transfer_amount);
	await scoreManipulation(interaction.user.id, -transfer_amount);
	await interaction.reply("success");
	await log("Issued: Command - Transfer", interaction.user.id);
}
