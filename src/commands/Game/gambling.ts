import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	CommandInteraction,
	ComponentType,
	SlashCommandBuilder,
} from "discord.js";
import { checkexist } from "../../utils/checkUserExist";
import { getScore } from "../../utils/getScore";
import { scoreManipulation } from "../../utils/scoreManipulation";
import { taxRate } from "../../utils/taxRate";
import { log } from "../../utils/logging";

export const data = new SlashCommandBuilder()
	.setName("gambling")
	.setDescription("Gamble higher or lower to other people")
	.addUserOption((option) =>
		option
			.setName("gambler")
			.setDescription("The user that you wish to gamble against!")
			.setRequired(true),
	)
	.addIntegerOption((option) =>
		option
			.setName("amount")
			.setDescription(
				"The amount of $ that you and the person you gamble with!",
			)
			.setRequired(true),
	);

export async function execute(interaction: CommandInteraction) {
	const gambler = interaction.options.getUser("gambler");
	const gambleAmount = interaction.options.getInteger("amount");
	if (gambler?.bot == true) {
		return interaction.reply(
			"Opss looks like you are trying to gamble with a bot. It's not going to be happening. HAHA",
		);
	}
	if (gambler?.id == interaction.user.id) {
		return interaction.reply(
			"You cannot gamble with yourself! Thats not how gambling works!",
		);
	}
	if ((await checkexist(gambler?.id)) != "yes") {
		return interaction.reply(
			`Opss looks like <@${gambler?.id}> has not register to the service. Try convince him to use /new-user to get registered now!`,
		);
	}
	if ((await checkexist(interaction.user.id)) != "yes") {
		return interaction.reply(
			`Opss looks like you have not register to the service. Try to use /new-user to get registered now!`,
		);
	}
	if (gambleAmount <= 0) {
		return interaction.reply(
			`Opss it looks like that chinlin saw some nasty bug. Perhaps don't. `,
		);
	}
	const gambler_original_amount = await getScore(gambler?.id);
	const user_original_amount = await getScore(interaction.user.id);
	if (gambler_original_amount < gambleAmount) {
		return interaction.reply(
			`Opss looks like <@${gambler?.id}> is broke. Perhaps try something lower.`,
		);
	}
	if (user_original_amount < gambleAmount) {
		return interaction.reply(
			`Opss looks like you are broke. Perhaps try something lower.`,
		);
	}
	const row = new ActionRowBuilder<ButtonBuilder>({
		components: [
			{
				custom_id: "approve",
				label: "✅️",
				style: ButtonStyle.Primary,
				type: ComponentType.Button,
			},
		],
	});
	const response = await interaction.reply({
		content: `<@${gambler?.id}> Press the check mark to approve the gambling! The person wants to gamble with $ ${gambleAmount} (Tax included). You have 60 second to approve this.`,
		components: [row],
	});
	const gambleAmountAT = Math.floor(
		gambleAmount * ((100 - taxRate.gamble_tax) / 100),
	);
	try {
		const confirmation = await response.awaitMessageComponent({
			time: 60_000,
		});
		if (
			confirmation.customId === "approve" &&
			confirmation.member?.user.id == gambler?.id
		) {
			await confirmation.reply("Start");
			await interaction.channel?.send("Higher or lower");
			await interaction.channel?.sendTyping();
			await interaction.channel?.send(
				`${interaction.user.displayName} gamble with ${gambler?.displayName}`,
			);
			await interaction.channel?.send("Here we go!!! 🎰");
			await interaction.channel?.sendTyping();
			let winner = Math.floor(Math.random() * 2);
			let winnerPot = Math.floor(Math.random() * (gambleAmountAT * 2 - 1));
			let playerA = 0;
			let playerB = 0;
			if (winner == 0) {
				playerA = gambleAmountAT * 2 - winnerPot;
				playerB = winnerPot;
			} else {
				playerB = gambleAmountAT * 2 - winnerPot;
				playerA = winnerPot;
			}
			if (playerA > playerB) {
				await interaction.channel?.send(
					`It seems like that ${interaction.user.displayName} have won`,
				);
				await interaction.channel?.send(`🥳🎉🎊`);
				await interaction.channel?.send(
					`${interaction.user.displayName} have: $${playerA} and ${gambler?.displayName} have: $${playerB}`,
				);
				await interaction.channel?.send(
					`maybe next time ${gambler?.displayName}`,
				);
				await scoreManipulation(interaction.user.id, playerA - gambleAmountAT);
				await scoreManipulation(gambler?.id, playerB - gambleAmountAT);
			} else {
				await interaction.channel?.send(
					`It seems like that ${gambler?.displayName} have won`,
				);
				await interaction.channel?.send(`🥳🎉🎊`);
				await interaction.channel?.send(
					`${interaction.user.displayName} have: ${playerA} and ${gambler?.displayName} have: ${playerB}`,
				);
				await interaction.channel?.send(
					`maybe next time ${interaction.user.displayName}`,
				);
				await scoreManipulation(interaction.user.id, playerA - gambleAmountAT);
				await scoreManipulation(gambler?.id, playerB - gambleAmountAT);
			}
			await log("Issued: Command - Gamble", interaction.user.id);
		} else if (confirmation.customId === "approve") {
			await confirmation.reply(
				`<@${confirmation.member?.user.id}> don't ruin the spirit of gambling, this button is meant for <@${gambler?.id}>`,
			);
			await interaction.deleteReply();
			await interaction.followUp("Gamble halt!");
		}
	} catch (err) {
		try {
			await interaction.editReply({
				content:
					"humm seems like the person have not press the check mark. Are you sure he wants to play!",
				components: [],
			});
		} catch (error) {
			await interaction.channel?.send({
				content:
					"humm seems like the person have not press the check mark. Are you sure he wants to play!",
				components: [],
			});
		}
		await interaction.followUp("Gamble halt!");
	}
}
