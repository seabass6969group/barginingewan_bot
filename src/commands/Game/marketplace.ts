import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import {
	exportOptionDiscord,
	getPriceItem,
	marketplaceItems,
} from "../../database/marketplaceItems";
import { scoreManipulation } from "../../utils/scoreManipulation";
import { openPacks } from "../../utils/game/openPacks";
import { getScore } from "../../utils/getScore";

export const data = new SlashCommandBuilder()
	.setName("marketplace")
	.setDescription("Browse the marketplace or buy items")
	.addSubcommand((option) =>
		option
			.setName("buy")
			.setDescription("buy items")
			.addStringOption((option) =>
				option
					.setName("item")
					.setDescription("Item to buy")
					.addChoices(...exportOptionDiscord())
					.setRequired(true),
			),
	)
	.addSubcommand((option) =>
		option.setName("browse").setDescription("browse items"),
	);
// .addSubcommand(option =>
//     option.setName("sell").setDescription("sell items")
// )

export async function execute(interaction: CommandInteraction) {
	switch (interaction.options.getSubcommand()) {
		case "buy":
			const getitem = interaction.options.getString("item");
			try {
				const getPrice = getPriceItem(getitem);
				if ((await getScore(interaction.user.id)) < getPrice) {
					throw new Error(
						"You might not have enough to even purchase this items!",
					);
				}
				interaction.reply("Please wait while we process your payment.");
				await openPacks(getitem, interaction.user.id);
				await scoreManipulation(interaction.user.id, -getPrice);
				interaction.channel?.send("You are all set");
				interaction.channel?.send(
					"To look at your purchased card please look at `/my-cards` ",
				);
			} catch (error) {
				interaction.channel?.send(`error: ${error}`);
				await interaction.deferReply();
			}
			break;
		case "browse":
			interaction.reply("Items to buy: ");
			marketplaceItems.forEach((value) => {
				interaction.channel?.send(
					` - ${value.name} $${value.price}: ${value.description}`,
				);
			});
			break;
	}
}
