import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	CommandInteraction,
	ComponentType,
	SlashCommandBuilder,
} from "discord.js";
import { scoreManipulation } from "../../../utils/scoreManipulation";
import { log } from "../../../utils/logging";
import { Cards, cards } from "../../../database/cards";
import { getOwnedCards } from "../../../utils/game/getCards";
function returnUrlPartly(card_type: string) {
	if (card_type == "AbilityCard") {
		return "ability";
	} else if (card_type == "MainCard") {
		return "main";
	} else if (card_type == "RizzCard") {
		return "rizz";
	}
	return "";
}
export const data = new SlashCommandBuilder()
	.setName("my-cards")
	.setDescription("your cards");

export async function execute(interaction: CommandInteraction) {
	try {
		const ownedCard = await getOwnedCards(interaction.user.id);
		const ownedCards = ownedCard as string[];
		if (ownedCards.length == 0) {
			return await interaction.reply("You don't have any cards");
		}
		let filtered: Cards = [];
		let original: Cards = [];
		ownedCards.forEach((s) => {
			filtered.push(JSON.parse(s));
			original.push(JSON.parse(s));
		});
		filtered = filtered.filter(
			(value, index, self) => index === self.findIndex((t) => t.id === value.id),
		);
		const row_nextonly = new ActionRowBuilder<ButtonBuilder>({
			components: [
				{
					custom_id: "next",
					label: "▶️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
				{
					custom_id: "all",
					label: "all",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
			],
		});
		const rows = new ActionRowBuilder<ButtonBuilder>({
			components: [
				{
					custom_id: "back",
					label: "◀️️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
				{
					custom_id: "next",
					label: "▶️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
				{
					custom_id: "all",
					label: "all",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
			],
		});
		const row_backonly = new ActionRowBuilder<ButtonBuilder>({
			components: [
				{
					custom_id: "back",
					label: "◀️️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
				{
					custom_id: "all",
					label: "all",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
			],
		});
		let idOn = 0;
		let cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
		let counts = 0;
		original.forEach((e) => {
			if (e.id == filtered[idOn].id) {
				counts++;
			}
		});
		const replyElement = await interaction.reply({
			content:
				`You currently have: ${counts}\nhttps://ewan.cephas.monster/cards/${cards_url_partly}/` +
				filtered[idOn].id +
				".png",
			components: [row_nextonly],
		});
		// console.log(filtered)
		try {
			while (true) {
				const confirmation = await replyElement.awaitMessageComponent({
					time: 60_000,
				});
				// console.log(confirmation.customId)
				confirmation.deferUpdate();
				if (confirmation.customId == "next") {
					if (idOn + 1 == filtered.length - 1) {
						idOn += 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
						let counts = 0;
						original.forEach((e) => {
							if (e.id == filtered[idOn].id) {
								counts++;
							}
						});
						await interaction.editReply({
							content:
								`You currently have: ${counts}\nhttps://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [row_backonly],
						});
						// console.log("https://ewan.cephas.monster/cards/${cards_url_partly}/")
					} else {
						idOn += 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
						let counts = 0;
						original.forEach((e) => {
							if (e.id == filtered[idOn].id) {
								counts++;
							}
						});
						interaction.editReply({
							content:
								`You currently have: ${counts}\nhttps://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [rows],
						});
					}
				} else if (confirmation.customId === "back") {
					if (idOn == 1) {
						idOn -= 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);

						let counts = 0;
						original.forEach((e) => {
							if (e.id == filtered[idOn].id) {
								counts++;
							}
						});
						interaction.editReply({
							content:
								`You currently have: ${counts}\nhttps://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [row_nextonly],
						});
					} else {
						idOn -= 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);

						let counts = 0;
						original.forEach((e) => {
							if (e.id == filtered[idOn].id) {
								counts++;
							}
						});
						interaction.editReply({
							content:
								`You currently have: ${counts}\nhttps://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [rows],
						});
					}
				} else if (confirmation.customId == "all") {
					interaction.editReply({
						content: "Please wait while we process all your cards that you have",
						components: [],
					});
					let final_string = "";
					filtered.forEach((a) => {
						let counts = 0;
						original.forEach((e) => {
							if (e.id == a.id) {
								counts++;
							}
						});
						if (final_string == "") {
							final_string = `- "${a.name}" as type of ${a.card_type}, ${counts} of them`;
						} else {
							final_string += `\n- "${a.name}" as type of ${a.card_type}, ${counts} of them`;
						}
					});
					return await interaction.editReply({ content: final_string });
				}
			}
		} catch (error) {
			try {
				interaction.editReply({
					content:
						`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
						filtered[idOn].id +
						".png",
					components: [],
				});
			} catch (error) {
				console.log(error);
			}
		}
	} catch (error) {
		return await interaction.reply("something seems to be broken can you try again")
	}
}
