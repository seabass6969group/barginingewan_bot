import { CommandInteraction, SlashCommandBuilder } from "discord.js";

export const data = new SlashCommandBuilder()
	.setName("deck")
	.setDescription("change your own deck")
	.addSubcommand((command) => command.setName("modify"));

export async function execute(interaction: CommandInteraction) {}
