import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	CommandInteraction,
	ComponentType,
	SlashCommandBuilder,
} from "discord.js";
import { scoreManipulation } from "../../../utils/scoreManipulation";
import { log } from "../../../utils/logging";
import { cards } from "../../../database/cards";
function returnUrlPartly(card_type: string) {
	if (card_type == "AbilityCard") {
		return "ability";
	} else if (card_type == "MainCard") {
		return "main";
	} else if (card_type == "RizzCard") {
		return "rizz";
	}
	return "";
}
export const data = new SlashCommandBuilder()
	.setName("cards")
	.setDescription("Ewan Card game info")
	.addSubcommand((option) =>
		option
			.setDescription("giving you info about the the card game")
			.setName("info"),
	)
	.addSubcommand((option) =>
		option
			.setName("search")
			.setDescription("search for cards")
			.addStringOption((option) =>
				option
					.setRequired(false)
					.setName("query")
					.setDescription("search query"),
			)
			.addStringOption((option) =>
				option
					.setRequired(false)
					.setName("rarity")
					.setDescription("search by rarity")
					.addChoices(
						{ name: "Godly", value: "Godly" },
						{ name: "Mythic", value: "Mythic" },
						{ name: "Legendary", value: "Legendary" },
						{ name: "Rare", value: "Rare" },
						{ name: "Uncommon", value: "Uncommon" },
						{ name: "Common", value: "Common" },
					),
			)
			.addStringOption((option) =>
				option
					.setRequired(false)
					.setName("card-type")
					.setDescription("search by card type")
					.addChoices(
						{ name: "ewan", value: "MainCard" },
						{ name: "ability", value: "AbilityCard" },
						{ name: "rizz", value: "RizzCard" },
					),
			),
	);

export async function execute(interaction: CommandInteraction) {
	if (interaction.options.getSubcommand() === "info") {
		interaction.reply("This is a game of ewan Card game");
		interaction.channel?.send("Coming soon!");
		interaction.channel?.send("But you can preview the cards.");
	}
	if (interaction.options.getSubcommand() === "search") {
		let rarity = interaction.options.getString("rarity");
		let card_type = interaction.options.getString("card-type");
		let query = interaction.options.getString("query");
		if (card_type == undefined && query == undefined && rarity == undefined) {
			return interaction.reply("Not typed anything");
		}
		const filtered = cards.filter((filter) => {
			if (
				(card_type == undefined || filter.card_type == card_type) &&
				(query == undefined ||
					filter.name.toLowerCase().includes(query.toLowerCase())) &&
				(rarity == undefined || filter.rarity == rarity)
			) {
				return filter;
			}
		});
		if (filtered.length == 0) {
			return interaction.reply("No card found");
		}

		const row_nextonly = new ActionRowBuilder<ButtonBuilder>({
			components: [
				{
					custom_id: "next",
					label: "▶️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
			],
		});
		const rows = new ActionRowBuilder<ButtonBuilder>({
			components: [
				{
					custom_id: "back",
					label: "◀️️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
				{
					custom_id: "next",
					label: "▶️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
			],
		});
		const row_backonly = new ActionRowBuilder<ButtonBuilder>({
			components: [
				{
					custom_id: "back",
					label: "◀️️",
					style: ButtonStyle.Primary,
					type: ComponentType.Button,
				},
			],
		});
		let idOn = 0;
		let cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
		if (filtered.length == 0) {
			return await interaction.reply({
				content:
					`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
					filtered[idOn].id +
					".png",
				components: [],
			});
		}
		const replyElement = await interaction.reply({
			content:
				`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
				filtered[idOn].id +
				".png",
			components: [row_nextonly],
		});
		// console.log(filtered)
		try {
			while (true) {
				const confirmation = await replyElement.awaitMessageComponent({
					time: 60_000,
				});
				// console.log(confirmation.customId)
				confirmation.deferUpdate();
				if (confirmation.customId == "next") {
					if (idOn + 1 == filtered.length - 1) {
						idOn += 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
						await interaction.editReply({
							content:
								`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [row_backonly],
						});
						// console.log("https://ewan.cephas.monster/cards/${cards_url_partly}/")
					} else {
						idOn += 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
						interaction.editReply({
							content:
								`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [rows],
						});
					}
				} else if (confirmation.customId === "back") {
					if (idOn == 1) {
						idOn -= 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
						interaction.editReply({
							content:
								`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [row_nextonly],
						});
					} else {
						idOn -= 1;
						cards_url_partly = returnUrlPartly(filtered[idOn].card_type);
						interaction.editReply({
							content:
								`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
								filtered[idOn].id +
								".png",
							components: [rows],
						});
					}
				}
			}
		} catch (error) {
			try {
				interaction.editReply({
					content:
						`https://ewan.cephas.monster/cards/${cards_url_partly}/` +
						filtered[idOn].id +
						".png",
					components: [],
				});
			} catch (error) {
				console.log(error);
			}
		}
	}
}
