import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	CommandInteraction,
	ComponentType,
	SlashCommandBuilder,
} from "discord.js";
import { getScore } from "../../../utils/getScore";
import { scoreManipulation } from "../../../utils/scoreManipulation";
import { eventtype, userEvent } from "../../../utils/event";
export const data = new SlashCommandBuilder()
	.setName("change-nickname")
	.setDescription("changing peoples nickname ($1000 required) (1 day only)")
	.addUserOption((option) =>
		option
			.setName("people")
			.setDescription("people you want to change the nickname to")
			.setRequired(true),
	)
	.addStringOption((option) =>
		option
			.setName("nickname")
			.setDescription(
				"The nickname you want to change the nickname to (cannot be offensive)",
			)
			.setRequired(true),
	);
export async function execute(interaction: CommandInteraction) {
	return await interaction.channel?.send({
		content: "The bot is temperary broken. Maybe one day it would come back.",
	});
	if (interaction.options.getUser("people") != undefined) {
		const currentScore = await getScore(interaction.user.id);
		if (currentScore > 1000) {
			await interaction.channel?.send({
				content: `You are about to change their nickname! $1000 is being charged! please accept the terms of service.`,
				ephemeral: true,
			});
			await interaction.channel?.send({
				content: `1. You must not use offensive words in the nickname.`,
			});
			const row = new ActionRowBuilder<ButtonBuilder>({
				components: [
					{
						custom_id: "approve",
						label: "✅️",
						style: ButtonStyle.Primary,
						type: ComponentType.Button,
					},
				],
			});
			const replyes = await interaction.reply({
				content: `Press the checkmark to perform the change`,
				components: [row],
			});
			try {
				const checkbox = await replyes.awaitMessageComponent({
					time: 60_000,
				});
				if (
					checkbox.customId === "approve" &&
					interaction.user.id === checkbox.member?.user.id
				) {
					const events = new userEvent(
						interaction.user.id,
						24,
						eventtype.nicknameChanger,
						interaction.options.getUser("people"),
					);
					const verification = await events.verification();
					if (verification == true) {
						await events.postRecords();
						await scoreManipulation(interaction.user.id, currentScore - 1000);
						return await interaction.channel?.send(`Successful`);
					} else {
						return await interaction.channel?.send(`Failed`);
					}
					///
				}
			} catch (error) {
				replyes.delete();
			}
		} else {
			return await interaction.reply(
				`You are broke. You cannot do this. You need at least $1000`,
			);
		}
	} else {
		return await interaction.reply(
			`You have not registered as a user. You cannot do this. You need at least $1000`,
		);
	}
}
