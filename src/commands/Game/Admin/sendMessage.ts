import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { log } from "../../../utils/logging";
export const data = new SlashCommandBuilder()
	.setName("sendmessage")
	.setDescription("send message")
	.addStringOption((option) =>
		option.setName("message").setDescription("message").setRequired(true),
	);

export async function execute(interaction: CommandInteraction) {
	const number_adds = interaction.options.getString("message");
	if (interaction.member.roles.cache.has("1208542738388357150")) {
		// if (interaction.user.id == "428949783919460363") {
		//     await log("Issued: Command - sendmessage", interaction.user.id)
		//     return await interaction.reply("lack of permission for doing this! Specially for chinlin!")
		// } else {
		await log("Issued: Command - sendmessage", interaction.user.id);
		return await interaction.reply(interaction.options.getString("message"));
		// }
	} else {
		return interaction.reply("lack of permission!");
	}
}
