import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { scoreManipulation } from "../../../utils/scoreManipulation";
import { log } from "../../../utils/logging";
export const data = new SlashCommandBuilder()
	.setName("appendscore")
	.setDescription("addscore")
	.addUserOption((option) =>
		option.setName("people").setDescription("people").setRequired(true),
	)
	.addIntegerOption((option) =>
		option.setName("score").setDescription("score").setRequired(true),
	);

export async function execute(interaction: CommandInteraction) {
	const number_adds = interaction.options.getInteger("score");
	if (interaction.member.roles.cache.has("1212070658515796050")) {
		// if (interaction.user.id == "428949783919460363" && number_adds <= 0) {
		//     await log("Issued: Command - appendScore", interaction.user.id)
		//     return await interaction.reply("lack of permission for decreasing people's balance! Specially for chinlin!")
		// } else {
		await log("Issued: Command - appendScore", interaction.user.id);
		await scoreManipulation(
			interaction.options.getUser("people")?.id,
			number_adds,
		);
		return await interaction.reply("success");
		// }
	} else if (interaction.member.roles.cache.has("1208542738388357150")) {
		if (number_adds <= 20 && number_adds >= -20) {
			await log("Issued: Command - appendScore", interaction.user.id);
			await scoreManipulation(
				interaction.options.getUser("people")?.id,
				number_adds,
			);
			return await interaction.reply("success");
		} else {
			await log(
				"Issued: Command - appendScore - STOPPED due to FRAUD",
				interaction.user.id,
			);
			return interaction.reply("fraudulent activity reported!");
		}
	} else {
		return interaction.reply("lack of permission!");
	}
}
