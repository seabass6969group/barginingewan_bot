import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { getScore } from "../../../utils/getScore";
export const data = new SlashCommandBuilder()
	.setName("search-score")
	.setDescription(
		"search score by individual people. Leaving people blank to shows you score",
	)
	.addUserOption((option) =>
		option.setName("people").setDescription("people").setRequired(false),
	);

export async function execute(interaction: CommandInteraction) {
	if (interaction.options.getUser("people") == undefined) {
		return await interaction.reply(
			`You have $ ${await getScore(interaction.user.id)}`,
		);
	} else {
		return await interaction.reply(
			`${interaction.options.getUser("people")?.displayName} has $ ${await getScore(interaction.options.getUser("people")?.id)}`,
		);
	}
}
