import { CommandInteraction, SlashCommandBuilder } from "discord.js";

export const data = new SlashCommandBuilder()
	.setName("interest-rate")
	.setDescription("show interest rate's info");

export async function execute(interaction: CommandInteraction) {
	return interaction.reply(
		"The interest rate is applied daily and it goes some where between 0.5% to 3.5%",
	);
}
