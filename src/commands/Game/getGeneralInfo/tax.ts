import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { taxRate } from "../../../utils/taxRate";

export const data = new SlashCommandBuilder()
	.setName("tax")
	.setDescription("show the tax info");

export async function execute(interaction: CommandInteraction) {
	interaction.reply("The tax rate are as followed");
	interaction.channel?.send(` - Gamble tax ${taxRate.gamble_tax}`);
	interaction.channel?.send(` - Transfer tax ${taxRate.transfer_tax}`);
}
