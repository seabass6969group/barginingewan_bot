import { CommandInteraction, SlashCommandBuilder } from "discord.js";
import { config } from "../../../config";

export const data = new SlashCommandBuilder()
	.setName("leaderboard")
	.setDescription("Shows you the leaderboard");

export async function execute(interaction: CommandInteraction) {
	const data = await fetch(
		"http://" + config.LOCAL_SERVER + "/api/getLeaderboard",
	);
	const response = await data.json();
	const real = response.test;
	let outputString = "Name | score \n";
	real.forEach((element) => {
		outputString += "- ";
		outputString += element.name;
		outputString += ": ";
		outputString += element.score;
		outputString += "\n";
	});
	return interaction.reply(outputString);
}
