export type chanceModifier = {
	Godly: number;
	Mythic: number;
	Legendary: number;
	Rare: number;
	Uncommon: number;
	Common: number;
};

interface marketplaceitem {
	name: string;
	price: number;
	description: string;
	value: string;
	rizz: boolean;
	main: boolean;
	ability: boolean;
	number: number;
	chance: chanceModifier;
}
const chance = {
	Godly: 1,
	Mythic: 2,
	Legendary: 10,
	Rare: 30,
	Uncommon: 100,
	Common: 200,
};
export const marketplaceItems: marketplaceitem[] = [
	{
		name: "Ewan Pack Ultimate",
		price: 1000,
		description: "25 packs (ability, Rizz, Ewan), ultimate luck",
		value: "ultimate_pack",
		rizz: true,
		ability: true,
		main: true,
		number: 25,
		chance: {
			Godly: 10,
			Mythic: 10,
			Legendary: 10,
			Rare: 10,
			Uncommon: 10,
			Common: 10,
		},
	},
	{
		name: "Ewan Pack Gold",
		price: 800,
		description: "15 packs (ability, Rizz, Ewan), Gold luck",
		value: "gold_pack",
		rizz: true,
		ability: true,
		main: true,
		number: 15,
		chance: {
			Godly: 5,
			Mythic: 7,
			Legendary: 15,
			Rare: 20,
			Uncommon: 30,
			Common: 50,
		},
	},
	{
		name: "Ewan Pack Sliver",
		price: 400,
		description: "10 packs (ability, Rizz, Ewan), good enough luck",
		value: "silver_pack",
		rizz: true,
		ability: true,
		main: true,
		number: 10,
		chance: {
			Godly: 2,
			Mythic: 4,
			Legendary: 7,
			Rare: 20,
			Uncommon: 30,
			Common: 50,
		},
	},
	{
		name: "Ewan Pack Basic",
		price: 200,
		description: "5 packs (ability, Rizz, Ewan), basic luck",
		value: "basic_pack",
		rizz: true,
		ability: true,
		main: true,
		number: 5,
		chance: {
			Godly: 1,
			Mythic: 4,
			Legendary: 50,
			Rare: 90,
			Uncommon: 150,
			Common: 200,
		},
	},
	{
		name: "Just Ewan Pack Basic",
		price: 100,
		description: "10 Ewan packs only",
		value: "basic_ewan",
		rizz: false,
		ability: false,
		main: true,
		number: 10,
		chance: {
			Godly: 1,
			Mythic: 1,
			Legendary: 1,
			Rare: 5,
			Uncommon: 10,
			Common: 20,
		},
	},
	{
		name: "Just Ewan Pack Bundle",
		price: 200,
		description: "15 Ewan packs only",
		value: "bundle_ewan",
		rizz: false,
		ability: false,
		main: true,
		number: 15,
		chance: {
			Godly: 1,
			Mythic: 2,
			Legendary: 4,
			Rare: 10,
			Uncommon: 15,
			Common: 20,
		},
	},
	{
		name: "Just Ewan Pack Bundle++",
		price: 350,
		description: "25 Ewan packs only",
		value: "plusplus_ewan",
		rizz: false,
		ability: false,
		main: true,
		number: 25,
		chance: {
			Godly: 2,
			Mythic: 5,
			Legendary: 6,
			Rare: 1,
			Uncommon: 1,
			Common: 1,
		},
	},
	{
		name: "Rizz Pack Basic",
		price: 100,
		description: "10 Rizz packs only",
		value: "basic_rizz",
		rizz: true,
		ability: false,
		main: false,
		number: 10,
		chance: {
			Godly: 1,
			Mythic: 1,
			Legendary: 1,
			Rare: 2,
			Uncommon: 2,
			Common: 5,
		},
	},
	{
		name: "Rizz Pack Bundle",
		price: 200,
		description: "15 Rizz packs only",
		value: "bundle_rizz",
		rizz: true,
		ability: false,
		main: false,
		number: 15,
		chance: {
			Godly: 1,
			Mythic: 1,
			Legendary: 1,
			Rare: 2,
			Uncommon: 2,
			Common: 5,
		},
	},
	{
		name: "Rizz Pack Bundle++",
		price: 350,
		description: "25 Rizz packs only",
		value: "plusplus_rizz",
		rizz: true,
		ability: false,
		main: false,
		number: 25,
		chance: {
			Godly: 1,
			Mythic: 1,
			Legendary: 1,
			Rare: 2,
			Uncommon: 2,
			Common: 5,
		},
	},
	{
		name: "Ability Pack Basic",
		price: 100,
		description: "5 Ability packs only",
		value: "basic_ability",
		rizz: false,
		ability: true,
		main: false,
		number: 5,
		chance: {
			Godly: 1,
			Mythic: 1,
			Legendary: 1,
			Rare: 2,
			Uncommon: 2,
			Common: 5,
		},
	},
	{
		name: "Ability Pack Bundle",
		price: 200,
		description: "10 Ability packs only",
		value: "bundle_ability",
		rizz: false,
		ability: true,
		main: false,
		number: 10,
		chance: {
			Godly: 1,
			Mythic: 1,
			Legendary: 1,
			Rare: 2,
			Uncommon: 2,
			Common: 5,
		},
	},
	{
		name: "Ability Pack Bundle++",
		price: 200,
		description: "25 Ability packs only",
		value: "plusplus_ability",
		rizz: false,
		ability: true,
		main: false,
		number: 25,
		chance: {
			Godly: 1,
			Mythic: 1,
			Legendary: 1,
			Rare: 2,
			Uncommon: 2,
			Common: 5,
		},
	},
];

export function exportOptionDiscord() {
	let arry: { name: string; value: string }[] = [];
	marketplaceItems.forEach((element) => {
		arry.push({ name: element.name, value: element.value });
	});
	return arry;
}
export function getPriceItem(item: string) {
	let finded = marketplaceItems.find((t) => t.value == item);
	if (finded != undefined) {
		return finded.price;
	} else {
		throw new Error("Item not found");
	}
}
