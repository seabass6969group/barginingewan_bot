import { AudioResource } from "@discordjs/voice";
import { User } from "discord.js";

interface Song {
	addedBy: User;
	audioResource: AudioResource<{ title: string }>;
}
export let songs: Song[] = [];
