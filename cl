<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="https://feeds.feedblitz.com/feedblitz_rss.xslt"?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0"
    xmlns:wotd="http://www.transparent.com/word-of-the-day/"
    xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0">
    <channel>
        <description>Transparent Language</description>
        <link>http://www.transparent.com/word-of-the-day/today/spanish.html</link>
        <title>Spanish Word of the Day</title>
        <pubDate>Mon, 26 Feb 2024 00:00:00 GMT</pubDate>
        <meta xmlns="http://www.w3.org/1999/xhtml" name="robots" content="noindex" />
        <item>
            <feedburner:origLink>
                http://www.transparent.com/word-of-the-day/today/spanish.html?date=02-26-2024</feedburner:origLink>
            <description><![CDATA[
<table>
<tr>
<th scope="row" style="color:#484868;font-family:Arial;font-size:large;text-align:left;vertical-align:top">Part of speech:</th>
<td style="color:#484868;font-family:Arial;font-size:large;padding-bottom:3em">verb</td>
</tr>
<tr>
<th scope="row" style="color:#484868;font-family:Arial;font-size:large;text-align:left;vertical-align:top">Example sentence:</th><td style="color:#484868;font-family:Arial;font-size:large;padding-bottom:3em">Los estudiantes dibujan al profesor en los cuadernos.</td>
</tr>
<tr>
<th scope="row" style="color:#484868;font-family:Arial;font-size:large;text-align:left;vertical-align:top">Sentence meaning:</th>
<td style="color:#484868;font-family:Arial;font-size:large;padding-bottom:3em">The students draw the teacher in their notebooks.</td>
</tr>
</table>
]]>
</description>
            <guid>http://www.transparent.com/word-of-the-day/today/spanish.html?date=02-26-2024</guid>
            <link>
                https://feeds.feedblitz.com/~/872183747/0/spanish-word-of-the-day~dibujar-to-draw.html</link>
            <pubDate>Mon, 26 Feb 2024 00:00:00 GMT</pubDate>
            <title><![CDATA[dibujar: to draw]]></title>
            <wotd:transliteratedWord></wotd:transliteratedWord>
            <wotd:transliteratedSentence></wotd:transliteratedSentence>
            <content:encoded><![CDATA[<table>
<tr>
<th scope="row" style="color:#484868;font-family:Arial;font-size:large;text-align:left;vertical-align:top">Part of speech:</th>
<td style="color:#484868;font-family:Arial;font-size:large;padding-bottom:3em">verb</td>
</tr>
<tr>
<th scope="row" style="color:#484868;font-family:Arial;font-size:large;text-align:left;vertical-align:top">Example sentence:</th><td style="color:#484868;font-family:Arial;font-size:large;padding-bottom:3em">Los estudiantes dibujan al profesor en los cuadernos.</td>
</tr>
<tr>
<th scope="row" style="color:#484868;font-family:Arial;font-size:large;text-align:left;vertical-align:top">Sentence meaning:</th>
<td style="color:#484868;font-family:Arial;font-size:large;padding-bottom:3em">The students draw the teacher in their notebooks.</td>
</tr>
</table>
<Img align="left" border="0" height="1" width="1" alt="" style="border:0;float:left;margin:0;padding:0;width:1px!important;height:1px!important;" hspace="0" src="https://feeds.feedblitz.com/~/i/872183747/0/spanish-word-of-the-day">
]]>
</content:encoded>
        </item>
    </channel>
</rss>